@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo starting... > %currentdir%..\..\logs\opsysupdates\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto next
) else (
	set Domain=yes
	goto next
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)
:next
echo Enabling auto update >> %currentdir%..\..\logs\opsysupdates\script.log
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update" -v AUOptions -t REG_DWORD -d 4 -f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -v AUOptions -t REG_DWORD -d 4 -f
echo disabling no update >> %currentdir%..\..\logs\opsysupdates\script.log
reg delete "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" /v NoAutoUpdate /f
echo starting and making windows update service auto >> %currentdir%..\..\logs\opsysupdates\script.log
sc config wuauserv start= auto >> nul 2>&1
sc start wuauserv >> nul 2>&1
echo running updatenow >> %currentdir%..\..\logs\opsysupdates\script.log
wuauclt.exe /updatenow
echo give me updates for other microsoft products >> %currentdir%..\..\logs\opsysupdates\script.log
powershell.exe -executionpolicy bypass -file %currentdir%files\opupdates.ps1
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1
exit