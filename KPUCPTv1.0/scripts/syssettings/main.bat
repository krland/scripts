@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo starting > %currentdir%..\..\logs\syssettings\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto next
) else (
	set Domain=yes
	goto next
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)
:next
echo setting reg keys >> %currentdir%..\..\logs\syssettings\script.log
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\CredUI" -v EnumerateAdministrators -t REG_DWORD -d 0 -f
reg add "HKCU\Software\Policies\Microsoft\Windows\Control Panel\Desktop" -v ScreenSaveActive -t REG_DWORD -d 1 -f
reg add "HKCU\Software\Policies\Microsoft\Windows\Control Panel\Desktop" -v ScreenSaverIsSecure -t REG_DWORD -d 1 -f
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" -v NoAutorun -t REG_DWORD -d 1 -f
echo enabling Data execution prevention >> %currentdir%..\..\logs\syssettings\script.log
bcdedit.exe /set {current} nx AlwaysOn
goto testcurl

:testcurl
if exist C:\Windows\System32\curl.exe goto urlgrabbing
goto curlinstall

:curlinstall
if %OS%==32BIT (
	copy "%currentdir%32\curl.exe" C:\Windows\System32
	goto testcurl
)
if %OS%==64BIT (
	copy "%currentdir%64\curl.exe" C:\Windows\System32
	goto testcurl
)

:urlgrabbing
type C:\CCS\README.url | find "URL=" > %currentdir%files\URLreadme.txt
for /F "tokens=*" %%D in (%currentdir%files\URLreadme.txt) do (
	set content=%%D
)
del %currentdir%files\URLreadme.txt
for /f "tokens=1,2 delims==" %%E in ("%content%") do (
	set noturl=%%E
	set url=%%F
)
:testurl
curl -k -s %url% | findstr -i /C:"Document Moved" >NUL
if %ERRORLEVEL% neq 0 goto curling
echo %url% | findstr -i "http:" >NUL
if %ERRORLEVEL% equ 0 (
set url=%url:http=https%
)
goto testurl

:curling
curl -k -s %url% > %currentdir%files\readmehtml1.txt
powershell.exe -executionpolicy bypass -file %currentdir%files\services.ps1
goto smbteststart

:smbteststart
type %currentdir%files\critservices.txt | findstr -i SMB
if %ERRORLEVEL% equ 0 (
	echo "SMB SHARE CRITICAL SERVICE, PLEASE CHECK FOR BAD SHARES BELOW!" >> %currentdir%..\..\logs\syssettings\script.log
	net share >> %currentdir%..\..\logs\syssettings\script.log
) else (
	echo deleting all shares >> %currentdir%..\..\logs\syssettings\script.log
	wmic path Win32_Share delete >> %currentdir%..\..\logs\syssettings\script.log
	reg delete "HKLM\SYSTEM\CurrentControlSet\Services\LanmanServer\Shares" /f
)
reg import %currentdir%files\AURN.reg
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1
exit