@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo starting... > %currentdir%..\..\logs\services\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto next
) else (
	set Domain=yes
	goto next
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)
:next
echo starting critical windows services
for %%S in (eventlog,mpssvc,Wecsvc,wersvc,windefend,sppsvc,wuauserv) do (
	echo Starting %%S and making it auto run >> %currentdir%..\..\logs\services\script.log
	sc config %%S start= auto >> nul 2>&1
	sc start %%S >> nul 2>&1
)
echo done.
for %%S in (rasman,rasauto,SSDPSRV,ftpsvc,iprip,tapisrv,bthserv,mcx2svc,snmptrap,remoteregistry,seclogon,telnet,tlntsvr,p2pimsvc,simptcp,fax,LPDSVC,msftpsvc,nettcpportsharing,iphlpsvc,lfsvc,upnphost,bthhfsrv,irmon,sharedaccess,xblauthmanager,xblgamesave,xboxnetapisvc) do (
	echo Stopping and disabling %%S >> %currentdir%..\..\logs\services\script.log
	sc config %%S start= disabled >> nul 2>&1
	sc stop %%S >> nul 2>&1
)
goto testcurl

:testcurl
if exist C:\Windows\System32\curl.exe goto urlgrabbing
goto curlinstall

:curlinstall
if %OS%==32BIT (
	copy "%currentdir%32\curl.exe" C:\Windows\System32
	goto testcurl
)
if %OS%==64BIT (
	copy "%currentdir%64\curl.exe" C:\Windows\System32
	goto testcurl
)

:urlgrabbing
type C:\CCS\README.url | find "URL=" > %currentdir%files\URLreadme.txt
for /F "tokens=*" %%D in (%currentdir%files\URLreadme.txt) do (
	set content=%%D
)
del %currentdir%files\URLreadme.txt
for /f "tokens=1,2 delims==" %%E in ("%content%") do (
	set noturl=%%E
	set url=%%F
)
:testurl
curl -k -s %url% | findstr -i /C:"Document Moved" >NUL
if %ERRORLEVEL% neq 0 goto curling
echo %url% | findstr -i "http:" >NUL
if %ERRORLEVEL% equ 0 (
set url=%url:http=https%
)
goto testurl

:curling
curl -k -s %url% > %currentdir%files\readmehtml1.txt
powershell.exe -executionpolicy bypass -file %currentdir%files\services.ps1
goto dns

:dns
type %currentdir%files\critservices.txt | findstr -i DNS>> nul 2>&1
if %ERRORLEVEL% equ 0 (
	echo "DNS Required, not touching it" >> %currentdir%..\..\logs\services\script.log
) else (
	echo Stopping DNS >> %currentdir%..\..\logs\services\script.log
	sc config DNS start= disabled >> nul 2>&1
	sc stop DNS >> nul 2>&1
)
goto iis

:iis
type %currentdir%files\critservices.txt | findstr -i IIS>> nul 2>&1
if %ERRORLEVEL% equ 0 (
	echo "IIS Required, not touching it" >> %currentdir%..\..\logs\services\script.log
) else (
	goto iis2
)
goto done

:iis2
type %currentdir%files\critservices.txt | findstr -i W3SVC>> nul 2>&1
if %ERRORLEVEL% equ 0 (
	echo "IIS Required, not touching it" >> %currentdir%..\..\logs\services\script.log
) else (
	echo stopping IIS >> %currentdir%..\..\logs\services\script.log
	sc config W3SVC start= disabled >> nul 2>&1
	sc stop W3SVC >> nul 2>&1
)
goto done

:done
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1
exit