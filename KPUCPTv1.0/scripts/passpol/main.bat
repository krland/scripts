@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo starting script... > %currentdir%..\..\logs\passpol\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto next
) else (
	set Domain=yes
	goto next
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)

:next
echo importing secedit.ini >> %currentdir%..\..\logs\passpol\script.log
secedit /configure /cfg %currentdir%files\secedit.ini /db secedit.sdb
echo imported >> %currentdir%..\..\logs\passpol\script.log
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1
exit