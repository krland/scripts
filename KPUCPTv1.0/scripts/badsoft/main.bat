@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo script starting > %currentdir%..\..\logs\badsoft\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto next
) else (
	set Domain=yes
	goto next
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)

:next
dir /b /s /A:SH "C:\Program Files\" > %currentdir%programfiles.flashed
if %OS% equ "64BIT" (
dir /b /s /A:SH "C:\Program Files (x86)\" >>%currentdir%programfiles.flashed
)
dir /b /s "C:\Program Files\" >>%currentdir%programfiles.flashed
if %OS% equ "64BIT" (
dir /b /s "C:\Program Files (x86)\" >>%currentdir%programfiles.flashed
)
setlocal EnableDelayedExpansion
set programs=cain,wireshark,john,mycleanpc,driversupport,httpexplorer,"Open TFTP Server",ophcrack,nmap,torrent,"web server",superscan,"chicken invaders",KNCTR,IRC,hashcat,tightvnc,bittor,port,itunes,teamviewer,ip,scanner,kodi,repair,driver,bitcomet,arcade,radmin
for %%i in (%programs%) do (
	findstr -i /C:%%i %currentdir%programfiles.flashed >nul
	if !ERRORLEVEL! equ 0 (
	echo found phrase "%%i" in programfiles >> %currentdir%..\..\logs\badsoft\script.log
	)
)
copy "%currentdir%programfiles.flashed" "%currentdir%..\..\logs\badsoft\programfiles.flashed"
endlocal
if %OS%==32BIT (
start %currentdir%files\RevoUn32.exe
) else (
start %currentdir%files\RevoUn64.exe
)
:done
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1