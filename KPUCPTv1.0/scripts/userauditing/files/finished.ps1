[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
$objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon 
$objNotifyIcon.Icon = "$PSScriptRoot\paul.ico"
$objNotifyIcon.BalloonTipIcon = "Info" 
$objNotifyIcon.BalloonTipText = "This notification means users are done!" 
$objNotifyIcon.BalloonTipTitle = "User Script, Finished!"
$objNotifyIcon.Visible = $True 
$objNotifyIcon.ShowBalloonTip(3000)
$objNotifyIcon.Dispose()