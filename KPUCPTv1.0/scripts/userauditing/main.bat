@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo script starting > %currentdir%..\..\logs\userauditing\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto testcurl
) else (
	set Domain=yes
	goto testcurl
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)
:testcurl
if exist C:\Windows\System32\curl.exe goto urlgrabbing
goto curlinstall

:curlinstall
if %OS%==32BIT (
	copy "%currentdir%32\curl.exe" C:\Windows\System32
	goto testcurl
)
if %OS%==64BIT (
	copy "%currentdir%64\curl.exe" C:\Windows\System32
	goto testcurl
)

:urlgrabbing
type C:\CCS\README.url | find "URL=" > %currentdir%files\URLreadme.txt
for /F "tokens=*" %%D in (%currentdir%files\URLreadme.txt) do (
	set content=%%D
)
del %currentdir%files\URLreadme.txt
for /f "tokens=1,2 delims==" %%E in ("%content%") do (
	set noturl=%%E
	set url=%%F
)
:testurl
curl -k -s %url% | findstr -i /C:"Document Moved" >NUL
if %ERRORLEVEL% neq 0 goto curling
echo %url% | findstr -i "http:" >NUL
if %ERRORLEVEL% equ 0 (
set url=%url:http=https%
)
goto testurl

:curling
curl -k -s %url% > %currentdir%files\readmehtml1.txt
goto usermanagementstartdomindetect

:usermanagementstartdomindetect
if %Domain%==no goto usermanagementstartnodomain
if %Domain%==yes goto usermanagementstartdomain

:usermanagementstartnodomain
powershell.exe -executionpolicy bypass -file %currentdir%files\grabauthadmins.ps1
wmic useraccount get name > %currentdir%files\allusers.txt
powershell.exe -executionpolicy bypass -file %currentdir%files\clearspace.ps1
mkdir %currentdir%files\ignore
powershell.exe -executionpolicy bypass -file %currentdir%files\delextra.ps1
powershell.exe -executionpolicy bypass -file %currentdir%files\grabauthusers.ps1
setlocal EnableDelayedExpansion
for /F "tokens=*" %%A in (%currentdir%files\Usersfinale.txt) do (
	find "%%A" %currentdir%files\authadmins.txt && (
	echo.
	) || (
		net localgroup administrators "%%A" /delete
		if !ERRORLEVEL! neq 2 (
			echo demoting unauthorized admin %%A >> %currentdir%..\..\logs\userauditing\script.log
		)
	)
)
for /F "tokens=*" %%A in (%currentdir%files\Usersfinale.txt) do (
	find "%%A" %currentdir%files\authusers.txt && (
	echo.
	) || (
		echo removing unauthorized user %%A >> %currentdir%..\..\logs\userauditing\script.log
		net user "%%A" /delete
	)
)
endlocal
goto passwordsettings

:usermanagementstartdomain
setlocal EnableDelayedExpansion
powershell.exe -executionpolicy bypass -file %currentdir%files\grabauthadmins.ps1
wmic useraccount get name > %currentdir%files\allusers.txt
powershell.exe -executionpolicy bypass -file %currentdir%files\clearspace.ps1
mkdir %currentdir%files\wind\ignore
powershell.exe -executionpolicy bypass -file %currentdir%files\delextra.ps1
powershell.exe -executionpolicy bypass -file %currentdir%files\grabauthusers.ps1

for /F "tokens=*" %%A in (%currentdir%files\Usersfinale.txt) do (
	find "%%A" %currentdir%files\authadmins.txt && (
	echo.
	) || (
		net localgroup administrators "%%A" /delete /domain
		if !ERRORLEVEL! neq 2 (
			echo demoting unauthorized admin %%A >> %currentdir%..\..\logs\userauditing\script.log
		)
	)
)
for /F "tokens=*" %%A in (%currentdir%files\Usersfinale.txt) do (
	find "%%A" %currentdir%files\authusers.txt && (
	echo.
	) || (
		net user "%%A" /delete /domain
		echo removing unauthorized user %%A >> %currentdir%..\..\logs\userauditing\script.log
	)
)
endlocal
goto passwordsettingsdomain

:passwordsettings
wmic useraccount get name > %currentdir%files\allusers.txt
powershell.exe -executionpolicy bypass -file %currentdir%files\clearspace.ps1
powershell.exe -executionpolicy bypass -file %currentdir%files\delextra.ps1
powershell.exe -executionpolicy bypass -file %currentdir%files\removeuserloggedin.ps1
echo Changing all Users Passwords to CyberPatriotRul3z!
for /F "tokens=*" %%A in (%currentdir%files\Userspasswords.txt) do (
	echo changing password for %%A >> %currentdir%..\..\logs\userauditing\script.log
	net user %%A CyberPatriotRul3z!
	echo making %%A's password expire >> %currentdir%..\..\logs\userauditing\script.log
	wmic UserAccount where Name="%%A" set PasswordExpires=true
	echo Enabling Password Required for %%A >> %currentdir%..\..\logs\userauditing\script.log
	wmic useraccount where name="%%A" set PasswordRequired=true
	echo unlocking %%A >> %currentdir%..\..\logs\userauditing\script.log
	wmic useraccount where name="%%A" set Lockout=false
	echo Enabling %%A >> %currentdir%..\..\logs\userauditing\script.log
	wmic useraccount where name="%%A" set Disabled=false
	echo Making %%A's Password changeable >> %currentdir%..\..\logs\userauditing\script.log
	wmic useraccount where name="%%A" set PasswordChangeable=true
)
goto creategroup

:passwordsettingsdomain
wmic useraccount get name > %currentdir%files\allusers.txt
powershell.exe -executionpolicy bypass -file %currentdir%files\clearspace.ps1
powershell.exe -executionpolicy bypass -file %currentdir%files\delextra.ps1
powershell.exe -executionpolicy bypass -file %currentdir%files\removeuserloggedin.ps1
echo Changing all Users Passwords to CyberPatriotRul3z!
for /F "tokens=*" %%A in (%currentdir%files\Userspasswords.txt) do (
	echo changing password for %%A >> %currentdir%..\..\logs\userauditing\script.log
	net user %%A CyberPatriotRul3z! /domain
	wmic UserAccount where Name="%%A" set PasswordExpires=true, PasswordRequired=true, Lockout=false, Disabled=false, PasswordChangeable=true
)
goto creategroup

:creategroup
CHOICE /C YN /M "Is there a group needed to add? (Y/N) "
if %ERRORLEVEL%==1 goto yescreategroup
if %ERRORLEVEL%==2 goto CreateUser

:yescreategroup
set /p makegroup="What is the name of the group you would like to make? "
if %Domain%==no (
	echo creating group %makegroup% >> %currentdir%..\..\logs\userauditing\script.log
	net localgroup %makegroup% /add
) else (
	echo creating group %makegroup% >> %currentdir%..\..\logs\userauditing\script.log
	net localgroup %makegroup% /add /domain
)
goto doaddusergroup

:doaddusergroup
CHOICE /c YN /M "Would you like to add a user to the new group? "
if %ERRORLEVEL%==1 goto adduserstogroup
if %ERRORLEVEL%==2 goto CreateUser

:adduserstogroup
set /p groupusersadd="What is the name of the user you would like to add to the new group? "
if %Domain%==no (
	echo adding %groupusersadd% to %makegroup% >> %currentdir%..\..\logs\userauditing\script.log
	net localgroup %makegroup% %groupusersadd% /add
) else (
	echo adding %groupusersadd% to %makegroup% >> %currentdir%..\..\logs\userauditing\script.log
	net localgroup %makegroup% %groupusersadd% /add /domain
)
goto doaddusergroup

:CreateUser
CHOICE /C YN /M "Is there a user you need to create? "
if %ERRORLEVEL%==1 goto yescreateuser
if %ERRORLEVEL%==2 goto adminpolicy

:yescreateuser
if %Domain%==no goto usertocreatenodomain
if %Domain%==yes goto usertocreatedomain

:usertocreatenodomain
set /p usertocreate="What is the name of the user you would like to create? "
echo Creating user %usertocreate% >> %currentdir%..\..\logs\userauditing\script.log
net user %usertocreate% CyberPatriotRul3z! /logonpasswordchg:yes /add /Y
goto CreateUser

:usertocreatedomain
set /p usertocreate="What is the name of the user you would like to create? "
echo Creating user %usertocreate% >> %currentdir%..\..\logs\userauditing\script.log
net user %usertocreate% CyberPatriotRul3z! /logonpasswordchg:yes /domain /add /Y
goto CreateUser

:adminpolicy
if %Domain%==no (
	echo Disabling Administrator account... >> %currentdir%..\..\logs\userauditing\script.log
	net user Administrator /active:no 
	echo Disabled administrator account >> %currentdir%..\..\logs\userauditing\script.log
	echo Disabling Guest account... >> %currentdir%..\..\logs\userauditing\script.log
	net user Guest /active:no
	echo Disabled guest account >> %currentdir%..\..\logs\userauditing\script.log
	echo Renaming Administrator to "Dude" and Guest to "LameDude" >> %currentdir%..\..\logs\userauditing\script.log
	powershell.exe -executionpolicy bypass -file "%currentdir%files\guestadminrename.ps1"
	echo Renamed Administrator to "Dude" and Guest to "LameDude" >> %currentdir%..\..\logs\userauditing\script.log
	goto done
) else (
	echo Disabling Administrator account... >> %currentdir%..\..\logs\userauditing\script.log
	net user Administrator /active:no /domain
	echo Disabled administrator account >> %currentdir%..\..\logs\userauditing\script.log
	echo Disabling Guest account... >> %currentdir%..\..\logs\userauditing\script.log
	net user Guest /active:no /domain
	echo Disabled guest account >> %currentdir%..\..\logs\userauditing\script.log
	echo Renaming Administrator to "Dude" and Guest to "LameDude" >> %currentdir%..\..\logs\userauditing\script.log
	powershell.exe -executionpolicy bypass -file "%currentdir%files\guestadminrename.ps1"
	echo Renamed Administrator to "Dude" and Guest to "LameDude" >> %currentdir%..\..\logs\userauditing\script.log
	goto done
)

:done
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1
exit