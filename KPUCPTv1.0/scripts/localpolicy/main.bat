@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo starting > %currentdir%..\..\logs\localpolicy\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto next
) else (
	set Domain=yes
	goto next
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)
:next
wmic useraccount get name > %currentdir%files\allusers.txt
powershell.exe -executionpolicy bypass -file %currentdir%files\clearspace.ps1
mkdir %currentdir%files\ignore
powershell.exe -executionpolicy bypass -file %currentdir%files\delextra.ps1
goto usrrights

:usrrights
echo Installing ntrights.exe to C:\Windows\System32 >> %currentdir%..\..\logs\localpolicy\script.log
copy %currentdir%files\ntrights.exe C:\Windows\System32
if exist C:\Windows\System32\ntrights.exe (
	echo Installation succeeded, managing user rights.. >> %currentdir%..\..\logs\localpolicy\script.log
	set remove=("Authenticated Users" "Backup Operators" "Guests" "Everyone" "Power Users" "Users" "NETWORK SERVICE" "LOCAL SERVICE" "Remote Desktop User" "ANONOYMOUS LOGON" "Guest" "Performance Log Users")
	for %%a in (%remove%) do (
			ntrights -U %%a -R SeNetworkLogonRight 
			ntrights -U %%a -R SeTrustedCredManAccessPrivilege
			ntrights -U %%a -R SeIncreaseQuotaPrivilege
			ntrights -U %%a -R SeInteractiveLogonRight
			ntrights -U %%a -R SeRemoteInteractiveLogonRight
			ntrights -U %%a -R SeSystemtimePrivilege
			ntrights -U %%a +R SeDenyNetworkLogonRight
			ntrights -U %%a +R SeDenyRemoteInteractiveLogonRight
			ntrights -U %%a -R SeProfileSingleProcessPrivilege
			ntrights -U %%a -R SeBatchLogonRight
			ntrights -U %%a -R SeCreateTokenPrivilege
			ntrights -U %%a -R SeUndockPrivilege
			ntrights -U %%a -R SeRestorePrivilege
			ntrights -U %%a -R SeShutdownPrivilege
			ntrights -U %%a -R SeTcbPrivilege
			ntrights -U %%a -R SeMachineAccountPrivilege
			ntrights -U %%a -R SeInteractiveLogonRight
			ntrights -U %%a -R SeBackupPrivilege
			ntrights -U %%a -R SeSystemtimePrivilege
			ntrights -U %%a -R SeCreatePagefilePrivilege
			ntrights -U %%a -R SeCreateGlobalPrivilege
			ntrights -U %%a -R SeCreatePermanentPrivilege
			ntrights -U %%a -R SeCreateSymbolicLinkPrivilege
			ntrights -U %%a -R SeDebugPrivilege
			ntrights -U %%a -R SeEnableDelegationPrivilege
			ntrights -U %%a -R SeRemoteShutdownPrivilege
			ntrights -U %%a -R SeAuditPrivilege
			ntrights -U %%a -R SeImpersonatePrivilege
			ntrights -U %%a -R SeIncreaseBasePriorityPrivilege
			ntrights -U %%a -R SeLoadDriverPrivilege
			ntrights -U %%a -R SeLockMemoryPrivilege
			ntrights -U %%a -R SeServiceLogonRight
			ntrights -U %%a -R SeSecurityPrivilege
			ntrights -U %%a -R SeRelabelPrivilege
			ntrights -U %%a -R SeSystemEnvironmentPrivilege
			ntrights -U %%a -R SeManageVolumePrivilege
			ntrights -U %%a -R SeSystemProfilePrivilege
			ntrights -U %%a -R SeAssignPrimaryTokenPrivilege
			ntrights -U %%a -R SeTakeOwnershipPrivilege
		)
		ntrights -U "Administrators" -R SeImpersonatePrivilege
		ntrights -U "Administrator" -R SeImpersonatePrivilege
		ntrights -U "SERVICE" -R SeImpersonatePrivilege
		ntrights -U "LOCAL SERVICE" +R SeImpersonatePrivilege
		ntrights -U "NETWORK SERVICE" +R SeImpersonatePrivilege
		ntrights -U "Administrators" +R SeMachineAccountPrivilege
		ntrights -U "Administrator" +R SeMachineAccountPrivilege
		ntrights -U "Administrators" +R SeIncreaseQuotaPrivilege
		ntrights -U "Administrator" -R SeIncreaseQuotaPrivilege
		ntrights -U "Administrators" -R SeDebugPrivilege
		ntrights -U "Administrator" -R SeDebugPrivilege
		ntrights -U "Adminisstrators" +R SeNetworkLogonRight
		ntrights -U "Administrators" +R SeLockMemoryPrivilege
		ntrights -U "Administrator" +R SeLockMemoryPrivilege
		ntrights -U "Administrators" -R SeBatchLogonRight
		ntrights -U "Administrator" -R SeBatchLogonRight
		echo Managed User Rights
		for /f "tokens=*" %%a in (%currentdir%files\Usersfinale.txt) do (
			ntrights -U %%a -R SeNetworkLogonRight 
			ntrights -U %%a -R SeTrustedCredManAccessPrivilege
			ntrights -U %%a -R SeIncreaseQuotaPrivilege
			ntrights -U %%a -R SeInteractiveLogonRight
			ntrights -U %%a -R SeRemoteInteractiveLogonRight
			ntrights -U %%a -R SeSystemtimePrivilege
			ntrights -U %%a +R SeDenyNetworkLogonRight
			ntrights -U %%a +R SeDenyRemoteInteractiveLogonRight
			ntrights -U %%a -R SeProfileSingleProcessPrivilege
			ntrights -U %%a -R SeBatchLogonRight
			ntrights -U %%a -R SeCreateTokenPrivilege
			ntrights -U %%a -R SeUndockPrivilege
			ntrights -U %%a -R SeRestorePrivilege
			ntrights -U %%a -R SeShutdownPrivilege
			ntrights -U %%a -R SeTcbPrivilege
			ntrights -U %%a -R SeMachineAccountPrivilege
			ntrights -U %%a -R SeInteractiveLogonRight
			ntrights -U %%a -R SeBackupPrivilege
			ntrights -U %%a -R SeSystemtimePrivilege
			ntrights -U %%a -R SeCreatePagefilePrivilege
			ntrights -U %%a -R SeCreateGlobalPrivilege
			ntrights -U %%a -R SeCreatePermanentPrivilege
			ntrights -U %%a -R SeCreateSymbolicLinkPrivilege
			ntrights -U %%a -R SeDebugPrivilege
			ntrights -U %%a -R SeEnableDelegationPrivilege
			ntrights -U %%a -R SeRemoteShutdownPrivilege
			ntrights -U %%a -R SeAuditPrivilege
			ntrights -U %%a -R SeImpersonatePrivilege
			ntrights -U %%a -R SeIncreaseBasePriorityPrivilege
			ntrights -U %%a -R SeLoadDriverPrivilege
			ntrights -U %%a -R SeLockMemoryPrivilege
			ntrights -U %%a -R SeServiceLogonRight
			ntrights -U %%a -R SeSecurityPrivilege
			ntrights -U %%a -R SeRelabelPrivilege
			ntrights -U %%a -R SeSystemEnvironmentPrivilege
			ntrights -U %%a -R SeManageVolumePrivilege
			ntrights -U %%a -R SeSystemProfilePrivilege
			ntrights -U %%a -R SeAssignPrimaryTokenPrivilege
			ntrights -U %%a -R SeTakeOwnershipPrivilege
		)
		ntrights -U "users" +R SeInteractiveLogonRight
		ntrights -U "Administrators" +R SeInteractiveLogonRight
		ntrights -U "Guest" +R SeDenyBatchLogonRight
		ntrights -U "Guest" +R SeDenyServiceLogonRight
		ntrights -U "Guest" +R SeDenyInteractiveLogonRight
		ntrights -U "Guest" +R SeDenyRemoteInteractiveLogonRight
		ntrights -U "Guest" +R SeDenyNetworkLogonRight
		ntrights -U "users" +R SeShutdownPrivilege
)
goto secoptions

:secoptions
echo Cleaning out the DNS cache... >> %currentdir%..\..\logs\localpolicy\script.log
ipconfig /flushdns
echo Writing over the hosts file... >> %currentdir%..\..\logs\localpolicy\script.log
attrib -r -s C:\WINDOWS\system32\drivers\etc\hosts
echo > C:\Windows\System32\drivers\etc\hosts
attrib +r +s C:\WINDOWS\system32\drivers\etc\hosts
REM Remove all saved credentials
cmdkey.exe /list > "%TEMP%\List.txt"
findstr.exe Target "%TEMP%\List.txt" > "%TEMP%\tokensonly.txt"
FOR /F "tokens=1,2 delims= " %%G IN (%TEMP%\tokensonly.txt) DO cmdkey.exe /delete:%%H
del "%TEMP%\*.*" /s /f /q
reg ADD HKCU\SYSTEM\CurrentControlSet\Services\CDROM /v AutoRun /t REG_DWORD /d 1 /f
reg ADD HKLM\SYSTEM\CurrentControlSet\Control\CrashControl /v CrashDumpEnabled /t REG_DWORD /d 0 /f
echo Add auditing to Lsass.exe >> %currentdir%..\..\logs\localpolicy\script.log
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\LSASS.exe" /v AuditLevel /t REG_DWORD /d 00000008 /f
echo Enable LSA protection >> %currentdir%..\..\logs\localpolicy\script.log
reg add HKLM\SYSTEM\CurrentControlSet\Control\Lsa /v RunAsPPL /t REG_DWORD /d 00000001 /f
echo The Windows Installer Always install with elevated privileges option must be disabled. >> %currentdir%..\..\logs\localpolicy\script.log
reg ADD HKLM\Software\Policies\Microsoft\Windows\Installer\ /v AlwaysInstallElevated /t REG_DWORD /d 0 /f
echo Network access: Restrict anonymous access to Named Pipes and Shares >> %currentdir%..\..\logs\localpolicy\script.log
reg ADD "HKLM\SYSTEM\CurrentControlSet\Services\LanManServer\Parameters" /v NullSessionShares /t REG_DWORD /d 1 /f
goto done

:done
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1