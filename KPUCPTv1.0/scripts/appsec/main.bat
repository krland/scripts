@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo script starting > %currentdir%..\..\logs\appsec\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto next
) else (
	set Domain=yes
	goto next
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)
:next
goto testcurl

:testcurl
if exist C:\Windows\System32\curl.exe goto urlgrabbing
goto curlinstall

:curlinstall
if %OS%==32BIT (
	copy "%currentdir%32\curl.exe" C:\Windows\System32
	goto testcurl
)
if %OS%==64BIT (
	copy "%currentdir%64\curl.exe" C:\Windows\System32
	goto testcurl
)

:urlgrabbing
type C:\CCS\README.url | find "URL=" > %currentdir%files\URLreadme.txt
for /F "tokens=*" %%D in (%currentdir%files\URLreadme.txt) do (
	set content=%%D
)
del %currentdir%files\URLreadme.txt
for /f "tokens=1,2 delims==" %%E in ("%content%") do (
	set noturl=%%E
	set url=%%F
)
:testurl
curl -k -s %url% | findstr -i /C:"Document Moved" >NUL
if %ERRORLEVEL% neq 0 goto curling
echo %url% | findstr -i "http:" >NUL
if %ERRORLEVEL% equ 0 (
set url=%url:http=https%
)
goto testurl

:curling
curl -k -s %url% > %currentdir%files\readmehtml1.txt
powershell.exe -executionpolicy bypass -file %currentdir%files\services.ps1
goto firefoxsec

:firefoxsec
echo securing firefox config >> %currentdir%..\..\logs\appsec\script.log
if exist "%programfiles%\Mozilla Firefox\" copy /Y "%currentdir%files\mozilla.cfg" "%programfiles%\Mozilla Firefox\"
if exist "%programfiles%\Mozilla Firefox\" copy /Y "%currentdir%files\local-settings.js" "%programfiles%\Mozilla Firefox\defaults\pref"
REM Install 64-bit customisations
if exist "%ProgramFiles(x86)%\Mozilla Firefox\" copy /Y "%currentdir%files\mozilla.cfg" "%ProgramFiles(x86)%\Mozilla Firefox\"
if exist "%ProgramFiles(x86)%\Mozilla Firefox\" copy /Y "%currentdir%files\local-settings.js" "%ProgramFiles(x86)%\Mozilla Firefox\defaults\pref"
goto IE

:IE
echo enabling Internet explorer >> %currentdir%..\..\logs\appsec\script.log
dism /online /Get-Features | findstr -i /C:"Internet-Explorer-Optional-x86" >nul
if %ERRORLEVEL% equ 0 (
	dism /online /Enable-Feature /FeatureName:Internet-Explorer-Optional-x86 /NoRestart
)
dism /online /Get-Features | findstr -i /C:"Internet-Explorer-Optional-amd64" >nul
if %ERRORLEVEL% equ 0 (
	dism /online /Enable-Feature /FeatureName:Internet-Explorer-Optional-amd64 /NoRestart
)
echo setting a bunch of IE security options >> %currentdir%..\..\logs\appsec\script.log
reg ADD "HKCU\Software\Microsoft\Internet Explorer\Main" /v DoNotTrack /t REG_DWORD /d 1 /f
reg ADD "HKCU\Software\Microsoft\Internet Explorer\Download" /v RunInvalidSignatures /t REG_DWORD /d 1 /f
reg ADD "HKCU\Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_LOCALMACHINE_LOCKDOWN\Settings" /v LOCALMACHINE_CD_UNLOCK /t REG_DWORD /d 1 /f
reg ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v WarnonBadCertRecving /t REG_DWORD /d 1 /f
reg ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v WarnOnPostRedirect /t REG_DWORD /d 1 /f
reg ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v WarnonZoneCrossing /t REG_DWORD /d 1 /f
reg ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v DisablePasswordCaching /t REG_DWORD /d 1 /f
reg ADD "HKLM\SOFTWARE\Policies\Microsoft\Internet Explorer\PhishingFilter" /v EnabledV9 /t REG_DWORD /d 1 /f
reg ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" /v SmartScreenEnabled /t REG_SZ /d RequireAdmin /f
reg ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows\System" /v EnableSmartScreen /t REG_DWORD /d 1 /f
reg ADD "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap" /v IEHarden /t REG_DWORD /d 1 /f
goto rdpchecking

:rdpchecking
type %currentdir%files\critservices.txt | findstr -i RDP
if %ERRORLEVEL% equ 0 (
	goto rdpyes
) else (
	goto remotedesktopchecking
)
:remotedesktopchecking
type %currentdir%files\critservices.txt | findstr -i /C:"Remote Desktop"
if %ERRORLEVEL% equ 0 (
	goto rdpyes
) else (
	goto rdpno
)
:rdpyes
echo RDP is a critical service, securing >> %currentdir%..\..\logs\appsec\script.log
echo enabling RDP >> %currentdir%..\..\logs\appsec\script.log
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Terminal Server" /v AllowTSConnections /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" /v fDenyTSConnections /t REG_DWORD /d 0 /f
REG ADD "HKLM\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f
echo disabling remote assistance >> %currentdir%..\..\logs\appsec\script.log
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Remote Assistance" /v fAllowToGetHelp /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" /v fAllowToGetHelp /t REG_DWORD /d 0 /f
echo Enabling Network Level Authentication >> %currentdir%..\..\logs\appsec\script.log
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp" /v UserAuthentication /t REG_DWORD /d 1 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" /v UserAuthentication /t REG_DWORD /d 1 /f
echo Requiring secure RDP communication >> %currentdir%..\..\logs\appsec\script.log
reg ADD "HKLM\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fEncryptRPCTraffic /t REG_DWORD /d 1 /f
red ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" /v fEncryptRPCTraffic /t REG_DWORD /d 1 /f
echo Enabling RDP TLS communication >> %currentdir%..\..\logs\appsec\script.log
reg ADD "HKLM\System\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp" /v SecurityLayer /t REG_DWORD /d 2 /f
echo disabling PNP Redirects >> %currentdir%..\..\logs\appsec\script.log
reg ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" /v fDisablePNPRedir /t REG_DWORD /d 1 /f
echo Always Prompt for password >> %currentdir%..\..\logs\appsec\script.log
reg ADD "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" /v fPromptForPassword /t REG_DWORD /d 1 /f
echo creating RDP Firewall rulle >> %currentdir%..\..\logs\appsec\script.log
netsh advfirewall firewall set rule group="remote desktop" new enable=yes
goto dnschecking

:rdpno
echo RDP is not a critical service, removing >> %currentdir%..\..\logs\appsec\script.log
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Terminal Server" /v AllowTSConnections /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" /v fDenyTSConnections /t REG_DWORD /d 1 /f
reg add "HKLM\SYSTEM\CurrentControlSet\Control\Remote Assistance" /v fAllowToGetHelp /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" /v fAllowToGetHelp /t REG_DWORD /d 0 /f
goto dnschecking

:dnschecking
type %currentdir%files\critservices.txt | findstr -i dns
if %ERRORLEVEL% equ 0 (
	goto dnsyes
) else (
	goto iischecking
)

:dnsyes
echo geting domain name >> %currentdir%..\..\logs\appsec\script.log
for /F %%i in ('wmic computersystem get domain ^| findstr -v Domain ^| findstr /v "^$"') do (
	set domname=%%i
	echo domain name is %domname% >> %currentdir%..\..\logs\appsec\script.log
)
echo securing DNS >> %currentdir%..\..\logs\appsec\script.log
echo Disabling DNS transfers to any server >> %currentdir%..\..\logs\appsec\script.log
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\DNS Server\Zones\%domname%" /v SecureSecondaries /t REG_DWORD /d 3 /f
echo Disabling dynamic updates >> %currentdir%..\..\logs\appsec\script.log
reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\DNS Server\Zones\%domname%" /v AllowUpdate /t REG_DWORD /d 0 /f
goto iischecking

:iischecking
type %currentdir%files\critservices.txt | findstr -i IIS
if %ERRORLEVEL% equ 0 (
	goto iisyes
) else (
	goto iischecking2
)
goto done

:iischecking2
type %currentdir%files\critservices.txt | findstr -i W3SVC
if %ERRORLEVEL% equ 0 (
	goto iisyes
) else (
	goto smbcheck
)
:iisyes
echo IIS is a service, securing >> %currentdir%..\..\logs\appsec\script.log
echo disabling IIS dir browsing >> %currentdir%..\..\logs\appsec\script.log
takeown /f C:\Windows\System32\inetsrv\dirlist.dll
del C:\Windows\System32\inetsrv\dirlist.dll
dism /online /disable-feature /featurename:IIS-DirectoryBrowsing /NoRestart
echo Removing IIS public errors >> %currentdir%..\..\logs\appsec\script.log
powershell -ep bypass -file %currentdir%files\iiserror.ps1
goto php

:php
for /F "tokens=2 delims=:" %%i in ('php -ini ^| findstr -i /c:"Loaded Configuration File"') do (
set phpinifile=C:%%i
)
echo enabling php log errors and disabling system function in %phpinifile% >> %currentdir%..\..\logs\appsec\script.log
echo log_errors=true >> %phpinifile%
echo disable_functions=system >> %phpinifile%
goto smbcheck

:smbcheck
type %currentdir%files\critservices.txt | findstr -i smb
if %ERRORLEVEL% equ 0 (
	goto smbyes
) else (
	goto done
)
:smbyes
echo SMB is a critical Service, securing>> %currentdir%..\..\logs\appsec\script.log
echo disabling smbv1 >> %currentdir%..\..\logs\appsec\script.log
Set-SmbServerConfiguration –EnableSMB1Protocol $false
echo Encrypting all shares >> %currentdir%..\..\logs\appsec\script.log
Set-SmbServerConfiguration –EncryptData $true
goto done

:done
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1