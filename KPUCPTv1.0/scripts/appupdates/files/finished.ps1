[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
$objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon 
$objNotifyIcon.Icon = "$PSScriptRoot\paul.ico"
$objNotifyIcon.BalloonTipIcon = "Info" 
$objNotifyIcon.BalloonTipText = "This notification means program updates are done!" 
$objNotifyIcon.BalloonTipTitle = "App Updates Script, Finished!"
$objNotifyIcon.Visible = $True 
$objNotifyIcon.ShowBalloonTip(3000)
$objNotifyIcon.Dispose()