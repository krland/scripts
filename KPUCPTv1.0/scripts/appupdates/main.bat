@echo off
title Windows Hardening Script
color 0b
cls
goto adminchecking

:adminchecking
net sessions
if %ERRORLEVEL%==0 (
echo script starting > %currentdir%..\..\logs\appupdates\script.log
wmic os get osarchitecture | find /i "32-bit" > NUL && set OS=32BIT || set OS=64BIT
set currentdir=%~dp0
systeminfo | findstr /B /C:"Domain" | findstr "WORKGROUP" > NUL
if %ERRORLEVEL%==0 (
	set Domain=no
	goto next
) else (
	set Domain=yes
	goto next
)
) else (
color 40
cls
echo ######## ########  ########   #######  ########  
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ##       ##     ## ##     ## ##     ## ##     ## 
echo ######   ########  ########  ##     ## ########  
echo ##       ##   ##   ##   ##   ##     ## ##   ##   
echo ##       ##    ##  ##    ##  ##     ## ##    ##  
echo ######## ##     ## ##     ##  #######  ##     ## 
echo.
echo.
echo ####### ERROR: ADMINISTRATOR PRIVILEGES REQUIRED #########
echo This script must be run as administrator to work properly!  
echo Run this as admin or ill kil ur entire family smh.
echo then right click it and select "Run As Administrator".
echo ##########################################################
echo.
pause
exit
)
:next
:testcurl
if exist C:\Windows\System32\curl.exe goto urlgrabbing
goto curlinstall

:curlinstall
if %OS%==32BIT (
	copy "%currentdir%32\curl.exe" C:\Windows\System32
	goto testcurl
)
if %OS%==64BIT (
	copy "%currentdir%64\curl.exe" C:\Windows\System32
	goto testcurl
)

:urlgrabbing
type C:\CCS\README.url | find "URL=" > %currentdir%files\URLreadme.txt
for /F "tokens=*" %%D in (%currentdir%files\URLreadme.txt) do (
	set content=%%D
)
del %currentdir%files\URLreadme.txt
for /f "tokens=1,2 delims==" %%E in ("%content%") do (
	set noturl=%%E
	set url=%%F
)
:testurl
curl -k -s %url% | findstr -i /C:"Document Moved" >NUL
if %ERRORLEVEL% neq 0 goto curling
echo %url% | findstr -i "http:" >NUL
if %ERRORLEVEL% equ 0 (
set url=%url:http=https%
)
goto testurl

:curling
curl -k -s %url% > %currentdir%files\readmehtml1.txt
powershell -ep bypass -file %currentdir%files\scenario.ps1
type %currentdir%files\prgms.txt | findstr -i "Firefox" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating firefox >> %currentdir%..\..\logs\appupdates\script.log
	taskkill /F /IM firefox.exe
	start %currentdir%files\ff.exe
)
type %currentdir%files\prgms.txt | findstr -i "Notepad++" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating notepad plus plus >> %currentdir%..\..\logs\appupdates\script.log
	taskkill /F /IM notepad++.exe
	start %currentdir%files\npp.exe
)
type %currentdir%files\prgms.txt | findstr -i "vlc" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating vlc >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\vlc.exe
)
type %currentdir%files\prgms.txt | findstr -i "filezilla" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating filezilla >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\fz.exe
)
type %currentdir%files\prgms.txt | findstr -i "TortoiseHG" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating tortoisehg >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\tthg.msi
)
type %currentdir%files\prgms.txt | findstr -i /C:"Reader DC" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating Adobe Acrobat Reader DC >> %currentdir%..\..\logs\appupdates\script.log
	powershell -ep bypass -file %currentdir%files\acro.ps1
)
type %currentdir%files\prgms.txt | findstr -i "libreoffice" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating libreoffice >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\libreoffice.exe
)
type %currentdir%files\prgms.txt | findstr -i "thunderbird" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating thunderbird >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\tb.exe
)
type %currentdir%files\prgms.txt | findstr -i /C:"JRE 8" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating java JRE 8 >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\java.exe
)
type %currentdir%files\prgms.txt | findstr -i "gimp" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating gimp >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\gimp.exe
)
type %currentdir%files\prgms.txt | findstr -i /C:"php 5.6" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating php 5.6>> %currentdir%..\..\logs\appupdates\script.log
	powershell -ep bypass -file %currentdir%files\php.ps1
)
type %currentdir%files\prgms.txt | findstr -i "putty" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating putty >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\putty.exe
)
type %currentdir%files\prgms.txt | findstr -i "geany" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating geany >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\geany.exe
)
type %currentdir%files\prgms.txt | findstr -i "jedit" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating jedit >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\jedit.exe
)
type %currentdir%files\prgms.txt | findstr -i "7-zip" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating 7z >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\7z.exe
)
type %currentdir%files\prgms.txt | findstr -i "ifranview" >NUL
if %ERRORLEVEL% equ 0 (
	echo updating ifranview >> %currentdir%..\..\logs\appupdates\script.log
	start %currentdir%files\ifran.exe
)
powershell.exe -executionpolicy bypass -file %currentdir%files\finished.ps1
exit