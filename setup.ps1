# Enable the running of scripts
#set-executionpolicy remotesigned

# Disable the running of scripts
#Set-ExecutionPolicy Restricted

# Create username files
#New-Item -Path C:\ -Name "allusers.txt" -ItemType file; New-Item -Path C:\ -Name "admins.txt" -ItemType file; New-Item -Path C:\ -Name "locals.txt" -ItemType file

# Export security policies
#secedit /export /cfg C:\securityconfig.cfg

# Import security policies
secedit /configure /db C:\Windows\security\new.sdb /cfg C:\scripts\securityconfig.cfg /areas SECURITYPOLICY

# Enable all audit policies
auditpol /set /category:* /failure:enable /success:enable

# Enable all firewalls
Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True

# Set some basic firewall rules
netsh advfirewall firewall set rule name="Remote Assistance (DCOM-In)" new enable=no 
netsh advfirewall firewall set rule name="Remote Assistance (PNRP-In)" new enable=no 
netsh advfirewall firewall set rule name="Remote Assistance (RA Server TCP-In)" new enable=no 
netsh advfirewall firewall set rule name="Remote Assistance (SSDP TCP-In)" new enable=no 
netsh advfirewall firewall set rule name="Remote Assistance (SSDP UDP-In)" new enable=no 
netsh advfirewall firewall set rule name="Remote Assistance (TCP-In)" new enable=no 
netsh advfirewall firewall set rule name="Telnet Server" new enable=no 
netsh advfirewall firewall set rule name="netcat" new enable=no

# Start windows update service
Set-Service wuauserv -StartupType Automatic
Start-Service wuauserv

# Get all the users who are supposed to be on the system from a file
$AllUsers = @(Get-Content -Path C:\scripts\allusers.txt)

# Get all the users who are supposed to be an admin on the system from a file
$Admins = @(Get-Content -Path C:\scripts\admins.txt)

# Get all the users who are supposed to be local users on the system from a file
$Locals = @(Get-Content -Path C:\scripts\locals.txt)

# Get all the users who are currently on the system
$CurrentUsers = @(Get-LocalUser | Select * | Select -ExpandProperty Name)

# Variable to mark if user exists in for loops
$Exists = 0

# Remove any users on the system who shouldn't be on the system
foreach ($User in $CurrentUsers) {
    $Exists = 0
    for ($i=0; $i -lt $AllUsers.Count; $i++) {
        if ($User -eq $AllUsers[$i]) {
            $Exists = 1
            break
        }
    }
    if ($Exists -eq 0) {
        Remove-LocalUser -Name $User -Confirm
    }
}

# Get all the users who are currently an admin on the system
$AlreadyAdmin = @(Get-LocalGroupMember -Name "Administrators" * | Select -ExpandProperty Name)

# Turn any users who should be an admin but aren't to an admin
foreach ($User in $Admins) {
    $Exists = 0
    for ($i=0; $i -lt $AlreadyAdmin.Count; $i++) {
        $Compare = "*" + $User
        if ($AlreadyAdmin[$i] -like $Compare) {
            $Exists = 1
            break
        }
    }
    if ($Exists -eq 0) {
        Add-LocalGroupMember -Group "Administrators" -Member $User
    }
}

# Remove any users who are an admin but should be a local user from admins
foreach ($User in $AlreadyAdmin) {
    $Exists = 0
    for ($i=0; $i -lt $Locals.Count; $i++) {
        $Compare = "*" + $Locals[$i]
        if ($User -like $Compare) {
            $Exists = 1
            break
        }
    }
    if ($Exists -eq 1) {
        Remove-LocalGroupMember -Group "Administrators" -Member $User -Confirm
    }
}

# Change every users password
$Password = ConvertTo-SecureString -String "5h/=bHXH6p" -AsPlainText -Force
foreach ($User in $AllUsers) {
    $User | Set-LocalUser -Password $Password
}

# Find all files that include the string rainbowcrack and delete them
$rainbow = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*rainbowcrack*" | % { $_.FullName })
foreach ($fp in $rainbow) {
    Remove-Item -Path $fp -Recurse
}

# Find all files that include the string england and delete them
$england = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*england*" | % { $_.FullName })
foreach ($fp in $england) {
    Remove-Item -Path $fp -Recurse
}

# Find all files that are the string nc.exe and delete them
$netcat = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "nc.exe" | % { $_.FullName })
foreach ($fp in $netcat) {
    Remove-Item -Path $fp -Recurse
}

# Find all files that are the string ncat.exe and delete them
$ncat = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "ncat.exe" | % { $_.FullName })
foreach ($fp in $ncat) {
    Remove-Item -Path $fp -Recurse
}

# Create Extensions directory if it doesn't already exist
if (-Not (Test-Path -Path C:\Extensions)) {
    New-Item -Path C:\ -Name "Extensions" -ItemType directory
}

# Find all txt files and write there filepath to a file
$txt = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.txt" | % { $_.FullName })
Write-Output $txt > C:\Extensions\txt.txt

# Find all mp3 files and write there filepath to a file
$mp3 = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.mp3" | % { $_.FullName })
Write-Output $mp3 > C:\Extensions\mp3.txt

# Find all zip files and write there filepath to a file
$zip = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.zip" | % { $_.FullName })
Write-Output $zip > C:\Extensions\zip.txt

# Find all csv files and write there filepath to a file
$csv = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.csv" | % { $_.FullName })
Write-Output $csv > C:\Extensions\csv.txt

# Find all bat files and write there filepath to a file
$bat = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.bat" | % { $_.FullName })
Write-Output $bat > C:\Extensions\bat.txt

# Find all exe files and write there filepath to a file
$exe = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.exe" | % { $_.FullName })
Write-Output $exe > C:\Extensions\exe.txt

# Find all gif files and write there filepath to a file
$gif = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.gif" | % { $_.FullName })
Write-Output $gif > C:\Extensions\gif.txt

# Find all jpeg files and write there filepath to a file
$jpeg = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.jpeg" | % { $_.FullName })
Write-Output $jpeg > C:\Extensions\jpeg.txt

# Find all jpg files and write there filepath to a file
$jpg = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.jpg" | % { $_.FullName })
Write-Output $jpg > C:\Extensions\jpg.txt

# Find all mp4 files and write there filepath to a file
$mp4 = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.mp4" | % { $_.FullName })
Write-Output $mp4 > C:\Extensions\mp4.txt

# Find all msi files and write there filepath to a file
$msi = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.msi" | % { $_.FullName })
Write-Output $msi > C:\Extensions\msi.txt

# Find all png files and write there filepath to a file
$png = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.png" | % { $_.FullName })
Write-Output $png > C:\Extensions\png.txt

# Find all php files and write there filepath to a file
$php = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.php" | % { $_.FullName })
Write-Output $php > C:\Extensions\php.txt

# Find all sh files and write there filepath to a file
$sh = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.sh" | % { $_.FullName })
Write-Output $sh > C:\Extensions\sh.txt

# Find all wav files and write there filepath to a file
$wav = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.wav" | % { $_.FullName })
Write-Output $wav > C:\Extensions\wav.txt

# Find all ps1 files and write there filepath to a file
$ps1 = @(Get-Childitem –Path C:\ -Recurse -ErrorAction SilentlyContinue -Include "*.ps1" | % { $_.FullName })
Write-Output $ps1 > C:\Extensions\ps1.txt

# Write all shares on the system to a file
$ComputerName = "\\" + $env:computername
net view $ComputerName /all > C:\shares.txt

# Write all tcp connections to a file
Get-NetTCPConnection > C:\ports.txt